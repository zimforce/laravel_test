<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;

    public function orders($order_id = null){

        return $this->hasMany(Order::class)->get();

        // return Order::where('client_id',$id)->get();

    }

    public function singleOrder($order_id){

        // $order = new Order();
        // return $order->where('client_id', $client_id)->where('id', $order_id)->get();

        return $this->hasOne(Order::class)->where('id', $order_id)->get();

    }


}
