<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    public function client(){

        return $this->belongsTo(Client::class)->get('name');

    }


    public function products(){

       return $this->belongsToMany(Product::class)->get();  

   }

   // public function quantity($order_id){

   //     return OrderProduct::where('order_id',$order_id)->get('quantity');  

   // }


}
