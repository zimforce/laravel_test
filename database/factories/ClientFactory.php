<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Order>
 */
class ClientFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [


            'name' => $this->faker->name(),
            // 'email' => $this->faker->sentence(1),
            'email' => $this->faker->unique()->safeEmail(),
            
            'password' => Str::random(10),


        ];
    }
}
