<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => $this->faker->unique()->word(),
            // 'price' => $this->faker->randomNumber(4, false),
            // 'price' => $this->faker->randomFloat(2, 10, 2000),
            'price' => random_int(10,2000),
            'description' => $this->faker->paragraph(3),
        ];
    }
}
