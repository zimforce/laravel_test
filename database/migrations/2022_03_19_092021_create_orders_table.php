<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();               //primary key
            $table->timestamps();
            // $table->unsignedBigInteger('client_id');
            // $table->foreign('client_id')->references('id')->on('clients');
            //or instead of 2 previous lines//  $table->foreignId('user_id')->constrained()->onDelete('cascade'); //foreign key
            $table->foreignId('client_id')->constrained()->onDelete('cascade'); //foreign key
            $table->string('status');

        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
};
