<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\Client::factory(10)->create();
        // \App\Models\Order::factory(30)->create();
        // \App\Models\Product::factory(100)->create();

         \App\Models\OrderProduct::factory(30)->create();

        // $affected = DB::table('phones')->insert(['phone' => random_int(100000000,999999999), 'name' => Str::random(10),]);

            // \App\Models\Client::factory()
            // ->count(50)
            // ->hasOrders(5)
            // ->create();

        

        
    }
}
