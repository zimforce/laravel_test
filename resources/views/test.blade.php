<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title></title>
</head>
<body>

<table>

	@foreach ($products as $product)

		<tr>
			<td><a href="/products/{{$product->id}}">{{$product->name}}</a></td>
			<td>{{$product->description}}</td>
			<td>{{$product->price}}</td>
		</tr>

	@endforeach

</table>	

</body>
</html>