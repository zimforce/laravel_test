<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});





Route::get('/orders', function () {
    $orders = \App\Models\Order::all();
    return $orders;
});


Route::get('/clients', function () {
    $users = \App\Models\Client::all();
    return $users;
});


Route::get('/clients/{id}/orders', function($id) {

    $client = new \App\Models\Client();

    $client = $client->find($id);

    return [

        'name' => $client['name'],

        'orders' => $client->orders(),

    ];


    // $clients = \App\Models\Client::where('id',$id)->get();
    // $clients = \App\Models\Client::all();

    // foreach ($clients as $client){

    //     echo $client['name'].'<br>';

    //     foreach ($client->orders() as $order){

    //     echo $order['id'].'<br>';            

    //     }

    // }

    // return $clients;

    // return response()->json(['data'=>$client]);

});





Route::get('/orders/{order:id}/client', function(\App\Models\Order $order) {

    return [

        'order_id' => $order['id'],

        'client_name'=> $order->client()

    ];

});


Route::get('/orders/{id}/products', function($id) {

    $order = \App\Models\Order::find($id);
    $products = $order->products();

        return [

        'order_id' => $order['id'],

        'products'=> $products

    ];

});


// Route::get('/clients/{client:id}/orders', function (\App\Models\Client $client) {

//     return $client;

//     // return response()->json(['data'=>$client]);

// });




Route::get('/users/{user:name}', function (\App\Models\User $user) {

    return $user;

});

