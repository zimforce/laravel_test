<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {
    return view('welcome');
});

Route::apiResources([
    '/rest/clients' => App\Http\Controllers\ClientController::class,
    '/rest/products' => App\Http\Controllers\ProductController::class,
    '/rest/orders' => App\Http\Controllers\OrderController::class,
    '/rest/clients.orders' => App\Http\Controllers\ClientOrderController::class,        // /rest/clients/{client}/order/{orders}
    // '/rest/orders.products' => App\Http\Controllers\ClientOrderController::class,
]);


// Route::apiResource('/rest/clients', \App\Http\Controllers\ClientController::class)->only([
//     'index', 'show'
//     // no create/store/edit/update/destroy
// ]);;


// Route::apiResource('/rest/products', \App\Http\Controllers\ProductController::class)->only([
//     'index', 'show'
//     // no create/store/edit/update/destroy
// ]);;







//------------------------------------------------------

//create product

Route::get('/create-product', function () {

    $product = \App\Models\Product::create([

        'name'=> 'laptop2',
        'description'=>'Lorem ipsum dolor',
        'price'=>'155.00'

    ]);

    return $product;

});



Route::get('/create-phone', function () {

    $num=0;
    while($num <= 10){

    DB::table('phones')->insert([
        'name' => Str::random(random_int(5,10)).' '.Str::random(random_int(8,12)),
        'phone' => random_int(100000000,200000000),
        'password' => Hash::make(Str::random(10)),
    ]);

    $num++;
    }

});


//view all products

Route::get('/products', function () {

    return view('test',['products' => \App\Models\Product::all()]);

});


//view a single product

Route::get('/products/{product:id}', function (\App\Models\Product $product) {

    return $product;

});

//view a single product 2

// Route::get('/products/{id}', function ($id) {

//     $product = \App\Models\Product::where('id',$id)->take(10)->orderByDesc('id')->get();

//     return $product;

// });


Route::get('/test', function(\Illuminate\Http\Request $request){

    return "1";

});